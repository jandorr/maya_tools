# README #

A collection of Maya Python tools from the "Introduction to Python Scripting in
Maya" course by Autodesk Scripting and SDK Learning Channel.

### Scripts ###

* randomCubes.py - Create and modify scene objects.
* randomInstances.py, aimAtFirst.py - How to work with the users current selection.
* keyRotation.py - Key framed animations.
* keyRotationWithUI.py - Create a simple UI.
* expandFromFirst.py - Generate expressions with custom attributes.

### Links ###

* [YouTube Tutorial](https://www.youtube.com/watch?v=eXFGeZZbMzQ&list=PL-4p6ppgFOkWtUtcp46Z_AufwVP0CougD&index=1)
