"""
expandFromFirst.py

Creates an "expansion" attribute on the first selected object which controls
the other selected objects distance from the first selected object.
"""

import maya.cmds as cmds


selectionList = cmds.ls(orderedSelection=True, type="transform")

if len(selectionList) >= 2:
    targetName = selectionList[0]
    selectionList.remove(targetName)
    locatorGroupName = cmds.group(empty=True, name="expansion_locator_grp#")

    # Create "expansion" attribute on target object.
    maxExpansion = 100
    newAttributeName = "expansion"
    if not cmds.objExists("{}.{}".format(targetName, newAttributeName)):
        cmds.select(targetName)
        cmds.addAttr(
            longName=newAttributeName,
            shortName="exp",
            attributeType="double",
            min=0,
            max=maxExpansion,
            defaultValue=maxExpansion,
            keyable=True,
        )

    for objectName in selectionList:
        coords = cmds.getAttr("{}.translate".format(objectName))[0]
        
        # Create locator.
        locatorName = cmds.spaceLocator(position=coords, name="{}_loc#".format(objectName))[0]
        
        # Center locator pivot.
        cmds.xform(locatorName, centerPivots=True)

        # Parent locator.
        cmds.parent(locatorName, locatorGroupName)

        # Constrain object between its locator and target.
        pointConstraintName = cmds.pointConstraint(
            [targetName, locatorName],
            objectName,
            name="{}_pointConstraint#".format(objectName)
        )[0]

        # Target Weight
        # Create an expression to evaluate the target object's attraction
        # weight. When "expansion" attribute is 100, the expression evaluates
        # to 0.
        cmds.expression(
            alwaysEvaluate=True,
            name="{}_attractWeight".format(objectName),
            object=pointConstraintName,
            string="{}W0={}-{}.{}".format(targetName, maxExpansion, targetName, newAttributeName)
        )

        # Locator Weight
        # Connect expansion attribute to drive the locator's point constraint
        # weight. When "expansion" attribute is 100, the locator has a weight
        # of 100.
        cmds.connectAttr(
            "{}.{}".format(targetName, newAttributeName),
            "{}.{}W1".format(pointConstraintName, locatorName)
        )

    # Center group pivot.
    cmds.xform(locatorGroupName, centerPivots=True)


else:
    print("Please select two or more objects.")
