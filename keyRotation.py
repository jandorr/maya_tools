"""
keyRotation.py

Key rotateY attribute for every selected object so that they all rotate 360
degrees within the current playback start and end time.
"""

import maya.cmds as cmds


def key_full_rotation(pObjectName, pStartTime, pEndTime, pTargetAttribute):
    # Delete rotateY animation keys from startTime to endTime.
    cmds.cutKey(pObjectName, time=(pStartTime, pEndTime), attribute=pTargetAttribute)
    cmds.setKeyframe(pObjectName, time=pStartTime, attribute=pTargetAttribute, value=0)
    cmds.setKeyframe(pObjectName, time=pEndTime, attribute=pTargetAttribute, value=360)
    # Select the attribute's keys in a specific time range.
    cmds.selectKey(pObjectName, time=(pStartTime, pEndTime), attribute=pTargetAttribute, keyframe=True)
    cmds.keyTangent(inTangentType="linear", outTangentType="linear")


selectionList = cmds.ls(selection=True, type="transform")
if selectionList:
    startTime = cmds.playbackOptions(query=True, minTime=True)
    endTime = cmds.playbackOptions(query=True, maxTime=True)

    for objectName in selectionList:
        key_full_rotation(objectName, startTime, endTime, "rotateY")
else:
    print("Please select at least one object")
