# keyRotationWithUI.py

import functools
import maya.cmds as cmds


def createUI(pWindowTitle, pApplyCallback):
    window_id = "myWindowID"

    # Close window if it's already open.
    if cmds.window(window_id, exists=True):
        cmds.deleteUI(window_id)

    # Create window.
    cmds.window(window_id, title=pWindowTitle, sizeable=False, resizeToFitChildren=True)
    cmds.rowColumnLayout(numberOfColumns=3, columnWidth=[(1, 75), (2, 60), (3, 60)], columnOffset=[(1, 'right', 3)])
    cmds.text(label="Time Range:")
    start_time_field = cmds.intField(value=cmds.playbackOptions(q=True, minTime=True))
    end_time_field = cmds.intField(value=cmds.playbackOptions(q=True, maxTime=True))
    cmds.text(label="Attribute:")
    target_attribute_field = cmds.textField(text="rotateY")
    cmds.separator(h=10, style="none")
    cmds.separator(h=10, style="none")
    cmds.separator(h=10, style="none")
    cmds.separator(h=10, style="none")
    cmds.separator(h=10, style="none")
    cmds.button(
        label="Apply",
        command=functools.partial(
            pApplyCallback,
            start_time_field,
            end_time_field,
            target_attribute_field
        )
    )

    def cancel_callback():
        if cmds.window(window_id, exists=True):
            cmds.deleteUI(window_id)

    cmds.button(label="Cancel", command=cancel_callback)
    cmds.showWindow()


def key_full_rotation(p_object_name, p_start_time, p_end_time, p_target_attribute):
    # Delete rotateY animation keys from startTime to endTime.
    cmds.cutKey(p_object_name, time=(p_start_time, p_end_time), attribute=p_target_attribute)
    cmds.setKeyframe(p_object_name, time=p_start_time, attribute=p_target_attribute, value=0)
    cmds.setKeyframe(p_object_name, time=p_end_time, attribute=p_target_attribute, value=360)
    # Select the attribute's keys in a specific time range.
    cmds.selectKey(p_object_name, time=(p_start_time, p_end_time), attribute=p_target_attribute, keyframe=True)
    cmds.keyTangent(inTangentType="linear", outTangentType="linear")


def apply_callback(p_start_time_field, p_end_time_field, p_target_attribute_field):
    start_time = cmds.intField(p_start_time_field, query=True, value=True)
    end_time = cmds.intField(p_end_time_field, query=True, value=True)
    target_attribute = cmds.textField(p_target_attribute_field, query=True, text=True)

    for objectName in cmds.ls(selection=True, type="transform"):
        key_full_rotation(objectName, start_time, end_time, target_attribute)


createUI("My Title", apply_callback)
