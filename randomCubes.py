# randomCubes.py
# Create random cube instances.

import random
import maya.cmds as cmds  # Provides most of the mel commands to Python.


random.seed(1234)

'''
cubeList = cmds.ls("myCube*")
if cubeList:
    cmds.delete(cubeList)
'''

result = cmds.polyCube(w=1.0, h=1.0, d=1.0, name="myCube#")
transformName = result[0]
instanceGroupName = cmds.group(empty=True, name=transformName + "_instance_grp#")

for i in range(0, 50):
    instanceResult = cmds.instance(transformName, name=transformName + "_instance#")
    cmds.parent(instanceResult, instanceGroupName)

    x = random.uniform(-10, 10)
    y = random.uniform(0, 20)
    z = random.uniform(-10, 10)
    cmds.move(x, y, z, instanceResult)

    xRot = random.uniform(0, 360)
    yRot = random.uniform(0, 360)
    zRot = random.uniform(0, 360)
    cmds.rotate(xRot, yRot, zRot, instanceResult)

    scalingFactor = random.uniform(0.3, 1.5)
    cmds.scale(scalingFactor, scalingFactor, scalingFactor, instanceResult)

cmds.hide(transformName)
cmds.xform(instanceGroupName, centerPivots=True)
